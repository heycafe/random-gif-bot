<?php

set_time_limit(180);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ini_set('date.timezone', 'Etc/GMT+8'); //--Is America/Vancouver With DTS AKA winter time in BC
ini_set('memory_limit','512M');

//##############################################################
//##############################################################-- Start Variable Fetching
//##############################################################

$system_timestamp=date('YmdHis');
$system_docroot=$_SERVER["DOCUMENT_ROOT"];
$system_apikey=makesafe($_GET['key']);

echo "<div>Welcome to the Hey.Café Bot</div>";

//##############################################################
//##############################################################-- Functions
//##############################################################

function makesafe($d){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","  ",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("`","&#96;",$d);
	return $d;
}

function fetchurl($url) {
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url,
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => false,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 5,
	));
	
	$return = curl_exec($curl);
	if (curl_error($curl)){
		return false;
	}
	curl_close($curl);
	return $return;
}

function heycafe_endpoint($function,$post=[]){
	//Example using post content
	//$post = [ 'content' => base64_encode("Hey look some content") ];
	//heycafe_endpoint("post_conversation_create",$post);
	
	global $system_apikey;
	$curl = curl_init();
	
	$postarray=array();
	$postarray["data"]=json_encode($post);
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => "https://endpoint.hey.cafe/".$function."?auth=".$system_apikey."",
		CURLOPT_SSL_VERIFYPEER  => false,
		CURLOPT_USERAGENT => 'CodeSimpleScript SSC Script HTTP_REQUEST',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => true,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 5,
		CURLOPT_POSTFIELDS => $postarray
	));
	
	$return = curl_exec($curl);
	if (curl_error($curl)){
		return false;
	}
	curl_close($curl);
	return $return;
}

function codegenerate($length=10){
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

//##############################################################
//##############################################################-- Check Notifications
//##############################################################


$notifications=heycafe_endpoint("get_account_notifications",['type' => 'mention', 'seen' => 'no']);
if ($notifications!=false){
	$mentions=json_decode($notifications,true);
	if ($mentions["system_api_error"]!=true){
		if (is_array($mentions["response_data"]["notifications"])){
			echo "<div>Got some notifications</div>";
			foreach ($mentions["response_data"]["notifications"] as $notification){
				if ($notification["seen"]=="0"){
					
					//--We have a notification, lets send a response to this mention
					echo "<div><strong>Mention:</strong> By ".$notification["from"]["name"]."</div>";
					
					//--Detect search query content by what was said.
					$search="";
					if ($notification["resource_sub"]=="false"){
						//--Conversation
						$content_query=heycafe_endpoint("get_conversation_info",[ 'query' => $notification["resource"] ]);
					}else{
						//--Comment
						$content_query=heycafe_endpoint("get_comment_info",[ 'query' => $notification["resource_sub"] ]);
					}
					$content=json_decode($content_query,true);
					if ($content["system_api_error"]!=true){
						$search=strtolower($content["response_data"]["contents"]);
						$search=trim(str_replace("@randomgif", "", $search));
						if (strlen($search)>=20){
							$search="";
						}
					}
					
					echo "<div><strong>Search</strong>: ".$search."</div>";
					
					//--Generate Content
					$content="";
					$tag="funny";
					if ($search!=""){
						$tag=$search;
					}
					$html=fetchurl("http://api.giphy.com/v1/gifs/search?q=".str_replace(' ', '+', $tag)."&api_key=kSzKnJExpT4z1MVvVPie1PK66qRwf4Ur&limit=10&rating=pg-13");
					$data=json_decode($html,true);
					if ($content==""){
						$rand_keys = array_rand($data["data"], 2);
						if (isset($data["data"][$rand_keys[0]]["title"])){
							$large=trim(makesafe($data["data"][$rand_keys[0]]["images"]["downsized"]["url"]));
							$large=preg_replace('/\?.*/', '', $large);
							if ($search==""){
								$content="Ok, here is a random GIF for you. ".$large."";
							}else{
								$content="Ok, here is a random GIF of '".$search."' for you. ".$large."";
							}
						}
					}
					
					if ($content==""){
						$html=fetchurl("http://api.giphy.com/v1/gifs/search?q=funny&api_key=kSzKnJExpT4z1MVvVPie1PK66qRwf4Ur&limit=10&rating=pg-13");
						$data=json_decode($html,true);
						if ($content==""){
							$rand_keys = array_rand($data["data"], 2);
							if (isset($data["data"][$rand_keys[0]]["title"])){
								$large=trim(makesafe($data["data"][$rand_keys[0]]["images"]["downsized"]["url"]));
								$large=preg_replace('/\?.*/', '', $large);
								$content="Ok, I was not able to find a GIF for '".$search."' so here is a random GIF. ".$large."";
							}
						}
					}
					
					if ($content==""){
						$content="Oh no, I cant find a GIF!!!!";
					}
					
					echo "<div><strong>Content</strong>: ".$content."</div>";
					heycafe_endpoint("post_comment_create",[ 'content' => base64_encode($content), 'conversation' => $notification["resource"], 'comment' => $notification["resource_sub"] ]);
					
					//--Mark as read so wont run again
					heycafe_endpoint("post_account_notification_seen",[ 'notification' => $notification["id"] ]);
				}
			}
		}else{
			echo "<div>We have no mentions</div>";
		}
	}else{
		echo "<div>Error on notification call: ".$mentions["system_api_error"]."</div>";
	}
}